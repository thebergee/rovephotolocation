import * as firebase from 'firebase';
import uuid from 'uuid';

import { subPinArray_d } from '../screens/AddTagsDrawer';
import { subPinsActive_d } from '../screens/AddTagsDrawer';

import { latitude_m } from '../screens/AddPinScreen';
import { longitude_m } from '../screens/AddPinScreen';


//var imUpload = 'https://www.gstatic.com/webp/gallery/2.jpg';

export var currentPin = '52_8956, -52_6749';
var textT = ' ';

//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
//https://stackoverflow.com/questions/38402025/how-to-create-helper-file-full-of-export functions-in-react-native

/**
 * Used in Add Tags Drawer
 */
export function setSuperAndSub_Active() {
  for (let i = 0; i < subPinArray_d.length; i++) {
    if (subPinArray_d[i].active == true) subPinsActive_d[i] = 1;
    else if (subPinArray_d[i].active == false) subPinsActive_d[i] = 0;
  }
}

export function printSuperSub_Active() {
  for (let i = 0; i < subPinArray_d.length; i++) {
    console.log(subPinArray_d[i].name + ': ' + subPinsActive_d[i]);
  }
}

/**
 * Used For Testing
 */
export function staticMethod() {
  return 'static method has been called.';
}

export function test() {
  return 'did this test work?';
}

/**
 * Used For View Pin Screen
 */
export function f() {
  console.log('>>' + this.yimUpload);
  return this.yimUpload;
}


export async function uploadImageAsync(uri) {
  ///// https://github.com/expo/expo/issues/2402#issuecomment-443726662////
  const blob = await new Promise((resolve, reject) => {
    const pass = new XMLHttpRequest();
    pass.onload = function() {
      resolve(pass.response);
    };
    pass.onerror = function(e) {
      console.log(e); //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++consoleLog
      reject(new TypeError('Network failed'));
    };
    pass.responseType = 'blob';
    pass.open('GET', uri, true);
    pass.send(null);
  });

  const ref = firebase
    .storage()
    .ref()
    .child(uuid.v4());
  const snapshot = await ref.put(blob);

  blob.close();

  return await snapshot.ref.getDownloadURL();
}

export async function t(targetpin) {
  let { image } = this.state;
  if (!image) {
    return;
  }
  //console.log(image)
  //console.log(get_ImgNum('eZ6KteqJT7PH7v0xVUTx') + "  //////////////////////////////////////////////////////")
  set_ImgNum(targetpin, get_ImgNum(targetpin) + 1);
  set_firebaseImgTest(targetpin, image, get_ImgNum(targetpin));
}

//////////////////////////////////////////////////////////////////////////firebaseImg
export function get_firebaseImgTest(userId, num, max) {

console.log(num + ": Num")

  console.log(num);
  if (max >= num && num >= 1) {
    console.log('works');
    
     return new Promise(function(resolve, reject) {
    setTimeout(() => {
  firebase
    .database()
    .ref('TestInfo1/' + userId + '/imgInfo' + num)
    .on('value', snapshot => {
      console.log("GIBS "+snapshot.val().imgInfo)
          resolve(snapshot.val().imgInfo);
          reject('https://www.gstatic.com/webp/gallery/1.jpg');
        });
    }, 200);
  });
  } else {
    console.log('Fail');
    return 'fail';
  }
  


 


}

export function set_firebaseImgTest(userId, img, num) {
  firebase
    .database()
    .ref('TestInfo1/' + userId + '/imgInfo' + num)
    .set({
      imgInfo: img,
    });
}
//////////////////////////////////////////////////////////////////////////userName
export function set_userName(userId, score) {
  firebase
    .database()
    .ref('TestInfo1/' + userId + '/nameInfo')
    .set({
      nameInfo: score,
    });
  //console.log("In thing ");//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++consoleLog
}

export function get_userName(userId) {
  firebase
    .database()
    .ref('TestInfo1/' + userId + '/nameInfo')
    .on('value', snapshot => {
      //console.log("Users Name: " + highscore);//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++consoleLog
    });
}
//////////////////////////////////////////////////////////////////////////ImgNum
export function set_ImgNum(userId, num) {
  firebase
    .database()
    .ref('TestInfo1/' + userId + '/imgNum')
    .set({
      imgNum: num,
    });
}

export function get_ImgNum(userId) {




  return new Promise(function(resolve, reject) {
    setTimeout(() => {
      firebase
        .database()
    .ref('TestInfo1/' + userId + '/imgNum')
    .on('value', snapshot => {

      console.log("GetImgNum : "+snapshot.val().imgNum)

          resolve(snapshot.val().imgNum);
          reject(2);
        });
    }, 200);
  });


}

//////////////////////////////////////////////////////////////////////////Pintypes
export function set_SuperPinType(targetPin, type) {
  {
    /*
default: 0
city:1
house:2
sea:3
forest:4
 */
  }

  firebase
    .database()
    .ref('TestInfo1/' + targetPin + '/SuperPinType')
    .set({
      superPinType: type,
    });
}

export function get_SuperPinType(targetPin) {
  return new Promise(function(resolve, reject) {
    setTimeout(() => {
      firebase
        .database()
        .ref('TestInfo1/' + targetPin + '/SuperPinType')
        .on('value', snapshot => {
          resolve(snapshot.val().superPinType);
          reject(0);
        });
    }, 200);
  });
}

export function set_SubPinType(targetPin, type, placement) {
  firebase
    .database()
    .ref('TestInfo1/' + targetPin + '/SubPinType' + placement)
    .set({
      subPinType: type,
    });
}

export function get_SubPinType(targetPin, placement) {
  return new Promise(function(resolve, reject) {
    setTimeout(() => {
      firebase
        .database()
        .ref('TestInfo1/' + targetPin + '/SubPinType' + placement)
        .on('value', snapshot => {
          resolve(snapshot.val().subPinType);
          reject(0);
        });
    }, 200);
  });
}

export async function set_NewPlace(
  x,
  y,
  superPin,
  sub0,
  sub1,
  sub2,
  sub3,
  sub4,
  sub5,
  sub6,
  sub7,
  sub8,
  sub9,
  img
) {
  var fixX = x.toString().replace('.', '_');
  var fixY = y.toString().replace('.', '_');

  {
    /*

"x,  y" will be the name of the place

superPin 
0 default
1 city
2 rural
3 coastal
4 wilderness

sub0 Architecture
sub1 Landmark
sub2 Beach
sub3 Graffiti
sub4 WildLife
sub5 Transport
sub6 Abandoned
sub7 Entertainment
sub8 Commercial
sub9 Residential

img is first image

 */
  }
  firebase
    .database()
    .ref('TestInfo1/' + fixX + ', ' + fixY + '/imgNum')
    .set({
      imgNum: 1,
    });

  var target = fixX + ', ' + fixY;

  set_SuperPinType(target, superPin);

  set_SubPinType(target, sub0, 0);
  set_SubPinType(target, sub1, 1);
  set_SubPinType(target, sub2, 2);
  set_SubPinType(target, sub3, 3);
  set_SubPinType(target, sub4, 4);
  set_SubPinType(target, sub5, 5);
  set_SubPinType(target, sub6, 6);
  set_SubPinType(target, sub7, 7);
  set_SubPinType(target, sub8, 8);
  set_SubPinType(target, sub9, 9);

  set_firebaseImgTest(target, img, 1);

  set_ElementsInFirebase(target, await get_LengthElementsInFirebase());
  set_LengthElementsInFirebase((await get_LengthElementsInFirebase()) + 1);
}

export function get_PlaceInfo(targetPin) {
  {
    /*

given a string of "x,y" it will print all of that pins info exept img

 */
  }
  var targetParts = targetPin.split(', ');

  console.log('X ' + targetParts[0]);
  console.log('Y ' + targetParts[1]);

  var fixX = targetParts[0].toString().replace('.', '_');
  var fixY = targetParts[1].toString().replace('.', '_');

  var fixtarget = fixX + ', ' + fixY;

  console.log('SuperPin ' + get_SuperPinType(fixtarget));

  console.log('SubPin 0:' + get_SubPinType(fixtarget, 0));
  console.log('SubPin 1:' + get_SubPinType(fixtarget, 1));
  console.log('SubPin 2:' + get_SubPinType(fixtarget, 2));
  console.log('SubPin 3:' + get_SubPinType(fixtarget, 3));
  console.log('SubPin 4:' + get_SubPinType(fixtarget, 4));
  console.log('SubPin 5:' + get_SubPinType(fixtarget, 5));
  console.log('SubPin 6:' + get_SubPinType(fixtarget, 6));
  console.log('SubPin 7:' + get_SubPinType(fixtarget, 7));
  console.log('SubPin 8:' + get_SubPinType(fixtarget, 8));
  console.log('SubPin 9:' + get_SubPinType(fixtarget, 9));
}

export async function checkForImgFail(num) {

  console.log(num);
  if (await get_ImgNum(currentPin) >= num && num >= 1) {
    console.log('works');
    return num;
  } else {
    return 1;
  }
  
}

export function get_ElementsInFirebase(num) {
  {
    /*

gets an element from PinList

num is its place in the list

 */
  }

  return new Promise(function(resolve) {
    setTimeout(() => {
      firebase
        .database()
        .ref('TestInfo1/' + 'PinList' + '/pin' + num)
        .on('value', snapshot => {
          resolve(snapshot.val().pinName);
        });
    }, 200);
  });
}

export function set_ElementsInFirebase(name, num) {
  {
    /*

adds a element to PinList

name should be the cordinates of the pin with "_" insted of "."

num is its place in the list (sugested to use get_LengthElementsInFirebase()+1)

 */
  }
  firebase
    .database()
    .ref('TestInfo1/' + 'PinList' + '/pin' + num)
    .set({
      pinName: name,
    });
}

export function get_LengthElementsInFirebase() {
  {
    /*

gets length of the list of pins in PinList 

 */
  }
  return new Promise(function(resolve) {
    setTimeout(() => {
      firebase
        .database()
        .ref('TestInfo1/' + 'PinList' + '/PinNum')
        .on('value', snapshot => {
          resolve(snapshot.val().pinNum);
        });
    }, 200);
  });
}

export function set_LengthElementsInFirebase(num) {
  {
    /*

sets length of the list of pins in PinList 

 */
  }
  firebase
    .database()
    .ref('TestInfo1/' + 'PinList' + '/PinNum')
    .set({
      pinNum: num,
    });
}

export async function get_PrintAllElementsInFirebase() {
  {
    /*

prints all items in PinList e.i all the pin cordinates

 */
  }
  var rList = [];
  var corHolder;

  for (var i = 0; i < (await get_LengthElementsInFirebase()); i++) {
    corHolder = await get_ElementsInFirebase(i).split(', ');
    rList.push({
      title: 'hello',
      coordinates: {
        latitude: corHolder[0],
        longitude: corHolder[1],
      },
    });
    console.log(await get_ElementsInFirebase(i));
  }

  //return rList----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}

export async function get_SearchSuperPinPrint(searchNum) {
  var rList = [];
  var corHolder;
  for (var i = 0; i < (await get_LengthElementsInFirebase()); i++) {
    if (searchNum == get_SuperPinType(get_ElementsInFirebase(i))) {
      corHolder = get_ElementsInFirebase(i).split(', ');
      rList.push({
        title: 'hello',
        coordinates: {
          latitude: corHolder[0],
          longitude: corHolder[1],
        },
      });
      console.log(get_ElementsInFirebase(i) + ' Works');
    }
  }
  //return rList----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}

export async function get_SearchSubPinPrint(
  sub0,
  sub1,
  sub2,
  sub3,
  sub4,
  sub5,
  sub6,
  sub7,
  sub8,
  sub9
) {
  var rList = [];
  var corHolder;
  for (var i = 0; i < (await get_LengthElementsInFirebase()); i++) {
    if (
      (sub0 == get_SubPinType(get_ElementsInFirebase(i), 0) && sub0 == 1) ||
      (sub1 == get_SubPinType(get_ElementsInFirebase(i), 1) && sub1 == 1) ||
      (sub2 == get_SubPinType(get_ElementsInFirebase(i), 2) && sub2 == 1) ||
      (sub3 == get_SubPinType(get_ElementsInFirebase(i), 3) && sub3 == 1) ||
      (sub4 == get_SubPinType(get_ElementsInFirebase(i), 4) && sub4 == 1) ||
      (sub5 == get_SubPinType(get_ElementsInFirebase(i), 5) && sub5 == 1) ||
      (sub6 == get_SubPinType(get_ElementsInFirebase(i), 6) && sub6 == 1) ||
      (sub7 == get_SubPinType(get_ElementsInFirebase(i), 7) && sub7 == 1) ||
      (sub8 == get_SubPinType(get_ElementsInFirebase(i), 8) && sub8 == 1) ||
      (sub9 == get_SubPinType(get_ElementsInFirebase(i), 9) && sub9 == 1)
    ) {
      corHolder = get_ElementsInFirebase(i).split(', ');
      rList.push({
        title: 'hello',
        coordinates: {
          latitude: corHolder[0],
          longitude: corHolder[1],
        },
      });
      console.log(get_ElementsInFirebase(i) + ' Works ');
    } else {
      console.log(get_ElementsInFirebase(i) + ' fail ');
    }
  }
  //return rList----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}

export async function get_SearchPinPrint(
  searchNum,
  sub0,
  sub1,
  sub2,
  sub3,
  sub4,
  sub5,
  sub6,
  sub7,
  sub8,
  sub9
) {
  textT = ' ';
  var rList = [];
  var corHolder;
  for (var i = 0; i < (await get_LengthElementsInFirebase()); i++) {
    if (
      searchNum == get_SuperPinType(get_ElementsInFirebase(i)) ||
      (sub0 == get_SubPinType(get_ElementsInFirebase(i), 0) && sub0 == 1) ||
      (sub1 == get_SubPinType(get_ElementsInFirebase(i), 1) && sub1 == 1) ||
      (sub2 == get_SubPinType(get_ElementsInFirebase(i), 2) && sub2 == 1) ||
      (sub3 == get_SubPinType(get_ElementsInFirebase(i), 3) && sub3 == 1) ||
      (sub4 == get_SubPinType(get_ElementsInFirebase(i), 4) && sub4 == 1) ||
      (sub5 == get_SubPinType(get_ElementsInFirebase(i), 5) && sub5 == 1) ||
      (sub6 == get_SubPinType(get_ElementsInFirebase(i), 6) && sub6 == 1) ||
      (sub7 == get_SubPinType(get_ElementsInFirebase(i), 7) && sub7 == 1) ||
      (sub8 == get_SubPinType(get_ElementsInFirebase(i), 8) && sub8 == 1) ||
      (sub9 == get_SubPinType(get_ElementsInFirebase(i), 9) && sub9 == 1)
    ) {
      corHolder = get_ElementsInFirebase(i).split(', ');
      rList.push({
        title: 'hello',
        coordinates: {
          latitude: corHolder[0],
          longitude: corHolder[1],
        },
      });

      textT = textT + corHolder[0] + ', ' + corHolder[1] + '|';

      console.log(get_ElementsInFirebase(i) + ' Works ');
    } else {
      console.log(get_ElementsInFirebase(i) + ' fail ');
    }
  }
  //return rList----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}
