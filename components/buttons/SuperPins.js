import * as React from 'react';
import {
  Text,
  View,
  TouchableWithoutFeedback,
} from 'react-native';
import {
  DISABLED_TOGGLE_BUTTON,
  ACTIVE_TOGGLE_BUTTON_C1,
  FONT_SIZE,
} from '../Styles';
import styles from '../Styles';
import * as Animatable from 'react-native-animatable';
import { Svg } from 'expo';


//ReactConference snack retrieved from
//https://github.com/expo/snack-web ?

class SuperPins extends React.Component {

  renderItem = (item) => {
    const { onPressItem, value } = this.props;
    return (
      <TouchableWithoutFeedback onPress={onPressItem.bind(this, item)}>
        <Animatable.View 
        style={[
          styles.button,
          {
            backgroundColor:
              item === value ? ACTIVE_TOGGLE_BUTTON_C1 : DISABLED_TOGGLE_BUTTON,
          },
        ]}
        key={item}>
        <Text style={{ color: 'white', fontSize: FONT_SIZE,fontFamily:'Montserrat-Regular',}}>
          {this.props.text}
          {item}
        </Text>
        </Animatable.View>
      </TouchableWithoutFeedback>
    );
  };

  render() {
    const { items } = this.props;
    return (
      <View style={styles.buttonContainer}>{items.map(this.renderItem)}</View>
    );
  }
}
export default SuperPins;