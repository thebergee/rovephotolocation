import React from 'react';
import { Text, TouchableWithoutFeedback
} from 'react-native';

import * as Animatable from 'react-native-animatable';

import {DISABLED_TOGGLE_BUTTON, ACTIVE_TOGGLE_BUTTON_C1,FONT_SIZE} from '../Styles'

/*
Retrieved : http://nobrok.com/create-react-native-animated-toggle-button-component/
*/

export default class SubPins extends React.Component {
  constructor(props) {
     super(props);
     this.state ={
       status: false,
       name:'someTag',
       isActive: 0,
     }
   }

_onPress(){
     this.props._onPress(!this.state.status)
     this.setState({ status: !this.state.status})
     if(this.state.status==true)
      {
          this.setState({isActive: 1})
      }
    else 
      this.setState({isActive: 0})
    this.refs.view.pulse(800)
   }
  render() 
  {
    return (
      <TouchableWithoutFeedback onPress={() => this._onPress()}>
        <Animatable.View ref="view" 
style={{
    alignItems: 'center',
    marginTop: 8.5,
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 8.5,
    marginRight: 8.5,
    borderRadius: 30,
    padding: 10,
    backgroundColor: this.state.status ? ACTIVE_TOGGLE_BUTTON_C1: DISABLED_TOGGLE_BUTTON }}>
          <Text style={{color: "white",fontFamily:'Montserrat-Regular',fontSize: FONT_SIZE}}>{this.props.text}</Text>

</Animatable.View>
      </TouchableWithoutFeedback>
    );
  }
}