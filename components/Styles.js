import React, { Component } from 'react';
import { Button, Image, Platform, View, Text, ImageBackground, Dimensions,TouchableOpacity,TouchableHighlight,Alert,StatusBar,StyleSheet} from 'react-native';

import * as Animatable from 'react-native-animatable';
import * as Font from 'expo-font';

//Colors
export const DISABLED_TOGGLE_BUTTON ="#666372";
export const ACTIVE_TOGGLE_BUTTON_C1 = '#46d2fd';
export const ACTIVE_TOGGLE_BUTTON_C2 = '#5351f0';

//Fonts
export const FONT_SIZE = 17;//body font default

export const BORDER_RADIUS = 30;

export const HEADER_COLOR='#0000';//'#1d1d27'

export default StyleSheet.create({
  container: {
    flex: 1
  },
  helpContainer: {
    flex: 10
  },
  viewImageContainer: {
    flex: 1,
    backgroundColor: 'black'
  },
  addContainer: {
    alignItems:'center',
    justifyContent:'center',
    height: Dimensions.get('screen').width/2,
    width: Dimensions.get('screen').width/2,
    backgroundColor: '#1C162E'
  },
  imageHolder: {
    height: Dimensions.get('screen').width/2,
    width: Dimensions.get('screen').width/2
  },
  elementsContainer: {
    height: Dimensions.get('screen').width/4,
    width: Dimensions.get('screen').width/4
  },
  homeindicator: {
    zIndex: 50,
    width: Dimensions.get('screen').width,
    height: 34,
    backgroundColor: '#E5DBF8'
  },
  layoutmargin: {
    position: 'relative',
    height: Dimensions.get('screen').height,
    width: 16,
    backgroundColor: '#C2EA9E'
    
  },
  buttonContainer: {
    position: 'relative',
  flexDirection: 'row',
  justifyContent: 'center',
  alignSelf:'center',
    flexWrap: 'wrap',
  },
  container_: {
    ...StyleSheet.absoluteFillObject,
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  button: {
    alignItems: 'center',
    marginTop: 8.5,
    paddingTop: 5,
    paddingBottom: 5,
    marginLeft: 8.5,
    marginRight: 8.5,
    borderRadius: 30,
    padding: 10,
    backgroundColor:'transparent',
    justifyContent: 'center',
  },
  baseText: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 40,
  },
  baseText_w: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 40,
    color: 'white',
  },
  elementsContainer_: {
    flex: 1,
    marginLeft: 24,
    marginRight: 24,
    marginBottom: 24
  },
  iPhoneHelpScreens:{
    height: 429,
    width: 214,
    flex: 1,
    alignItems: 'center',
    marginVertical: 20,
    paddingBottom: 20
  },
  rove_headersty:{
    height: 35,
    width: 130,
    flex: 1,
    resizeMode:'contain',
  },
  statusbarSty:{
    zIndex: 10000,
    justifyContent: 'center',
    alignSelf: 'center',
    paddingBottom: 5,
    position: 'absolute',
    backgroundColor:'transparent',
    top: Dimensions.get('screen').height - 350,
  },
  roveLogo_Loading:{
    zIndex: 10001,
    justifyContent: 'center',
    top: Dimensions.get('screen').height - 600,
    alignSelf: 'center',
    backgroundColor:'transparent',
    position: 'absolute'
  },
  loadingText:{
    zIndex: 10001,
    justifyContent: 'center',
    top: Dimensions.get('screen').height - 400,
    alignSelf: 'center',
    backgroundColor:'transparent',
    position: 'absolute'
  },
  baseText_loading: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 30,
    color: 'white'
  },
  container_loading: {
    position: 'absolute',
    justifyContent: 'center',
    height: Dimensions.get('screen').height,
    width: Dimensions.get('screen').width
  },
  curtain_loading: {
    position: 'absolute',
    zIndex: 100,
    backgroundColor: 'black',
    opacity: 0.75,
    height: Dimensions.get('screen').height,
    width: Dimensions.get('screen').width
  },
  labels:{
    position: 'relative',
    color: '#4E4E4F',
    fontFamily: 'Montserrat-Regular',
    fontSize: 22
  },
  buffer:
  {
    position: 'relative',
    marginTop: 10,
    marginBottom: 5,
    height: 10,
    backgroundColor: 'white'
  }
});
//'#E5DBF8'
//baseText and base_Text_w doesn't seem to work