import React from 'react';
import {
  Button,
  View,
  TouchableOpacity,
  StatusBar,
  ScrollView,
} from 'react-native';

import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'

import Icon from 'react-native-vector-icons/FontAwesome';

import AsyncImage from '../components/AsyncImage';

import styles from '../components/Styles';
import Svg, {Rect} from 'react-native-svg';

import { superTypeSelectedVal_d } from './AddTagsDrawer';
import { subPinsActive_d } from './AddTagsDrawer';

import { latitude_m } from './AddPinScreen';
import { longitude_m } from './AddPinScreen';

//importing functions
import { set_NewPlace, uploadImageAsync } from '../components/Functions';

/**
 * Add Pin -> TagDrawer -> ViewPinImages -> Loading -> Home
 */

var imUpload = 'https://www.gstatic.com/webp/gallery/3.jpg';

class ViewPinImagesScreen extends React.Component {
  state = {
    bodyText: 'T',
    bodyTextTwo: ' ',
    image: null,
    yimUpload: 'https://www.gstatic.com/webp/gallery/4.jpg',
    imageUri: 'T',
  };

  async componentDidMount() {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    console.log('in_componentDidMount()');
  }

  setfirstImgtoScreen() {
    console.log('in_setfirstImgtoScreen()');
    this.setState({ imageUri: imUpload });
    console.log(this.state.imageUri);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Gallery',
      headerRight: (
        <Button
          onPress={() => {
            set_NewPlace(
              latitude_m,
              longitude_m,
              superTypeSelectedVal_d, //super
              subPinsActive_d[0], //subpins
              subPinsActive_d[1],
              subPinsActive_d[2],
              subPinsActive_d[3],
              subPinsActive_d[4],
              subPinsActive_d[5],
              subPinsActive_d[6],
              subPinsActive_d[7],
              subPinsActive_d[8],
              subPinsActive_d[9],
              imUpload
            ),
              navigation.navigate('Home');
          }}
          title="Done"
          color="#fff"
        />
      ),
    };
  };

  render() {
    return (
      <ScrollView style={styles.viewImageContainer}>
        <StatusBar barStyle="light-content" />
        <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
          <View style={styles.addContainer}>
            <Svg width="200" height="200">
              <Rect
                x="5%"
                y="5%"
                width="90%"
                height="90%"
                fill="black"
                strokeWidth="4"
                stroke="white"
                strokeDasharray="5,5"
              />
            </Svg>
            <View
              style={[
                {
                  position: 'absolute',
                  zIndex: 100,
                },
              ]}>
              <TouchableOpacity onPress={this._BBpickImage}>
                <Icon
                  name={'plus-circle'}
                  color={'#0A89C2'}
                  backgroundColor={'transparent'}
                  size={70}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[
                {
                  position: 'absolute',
                  zIndex: 1,
                },
              ]}>
              <Icon
                name={'circle'}
                color={'white'}
                style={'solid'}
                backgroundColor={'pink'}
                size={70}
              />
            </View>
          </View>
          <AsyncImage
            style={styles.addContainer}
            source={{
              uri: this.state.imageUri,
            }}
            placeholderColor='black'
          />
        </View>
      </ScrollView>
    );
  }

  _firstimage = () => {
    let { image } = this.state;
    if (!image) {
      console.log('https://www.gstatic.com/webp/gallery/2.jpg');
      return;
    } else {
      return image;
    }
  };

  _pickImage = async () => {
    console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    this._handleImagePicked(pickerResult);
  };

  _handleImagePicked = async pickerResult => {
    console.log('wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww');
    try {
      this.setState({ uploading: true });

      if (!pickerResult.cancelled) {
        //  console.log("in thing");
        var uploadUrl = await uploadImageAsync(pickerResult.uri);
        this.setState({ image: uploadUrl });
      }
    } catch (e) {
      console.log(e);
      alert('Something went wrong in upload');
    } finally {
      this.setState({ uploading: false });
    }
  };

  _BBpickImage = async () => {
    console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxx');
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4,3],
    });
    this._BBhandleImagePicked(pickerResult);
  };

  _BBhandleImagePicked = async pickerResult => {
    console.log('nnnnnnnnnnnnnnnnnnnnnnnnnnn');
    try {
      this.setState({ uploading: true });

      if (!pickerResult.cancelled) {
        //  console.log("in thing");
        var uploadUrl = await uploadImageAsync(pickerResult.uri);
        this.setState({ image: uploadUrl });
        this.setState({ yimUpload: uploadUrl });
        imUpload = uploadUrl;

        console.log(uploadUrl);
        {
          this.setfirstImgtoScreen();
        }
        {
          this.componentDidMount();
        }
      }
    } catch (e) {
      console.log(e);
      alert('Something went wrong in upload');
    } finally {
      this.setState({ uploading: false });
    }
  };
}

export default ViewPinImagesScreen;