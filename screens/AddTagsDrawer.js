import React from 'react';
//import React from 'react';
import {
  Button,
  View,
  Text,
  StatusBar,
} from 'react-native';


//import {xCoord} from './AddPinScreen';
//import {yCoord} from './AddPinScreen';

import SubPins from '../components/buttons/SubPins';
import SuperPins from '../components/buttons/SuperPins';
import styles from '../components/Styles';


var superTypeSelected_d = '  Other  ';
var superTypeSelectedVal_d = 0;

//0-9
var subPinsActive_d = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

var subPinArray_d = [
  {
    //Architecture
    name: '  Architecture  ',
    active: false,
  },
  {
    //Landmark
    name: '  Landmark  ',
    active: false,
  },
  {
    //Beach
    name: '  Beach  ',
    active: false,
  },
  {
    //Graffiti
    name: '  Graffiti  ',
    active: false,
  },
  {
    //WildLife
    name: '  WildLife  ',
    active: false,
  },
  {
    //Transport
    name: '  Transport  ',
    active: false,
  },
  {
    //Abandoned
    name: '  Abandoned  ',
    active: false,
  },
  {
    //Entertainment
    name: '  Entertainment  ',
    active: false,
  },
  {
    //Commercial
    name: '  Commercial  ',
    active: false,
  },
  {
    //Residential
    name: '  Residential  ',
    active: false,
  },
];


class AddTagsDrawer extends React.Component {
  state = {
    superType: '  Other  ',
  };
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Add Tags',
      headerRight: (
        <Button
          onPress={() => {
            //navigation.navigate('Temp')}
            navigation.navigate('View_pin_images');
          }}
          title="Next"
          color="#fff"
        />
      ),
    };
  };

  handlePressItem = item => {
    this.setState({ superType: item });
    superTypeSelected_d = item;
    switch (superTypeSelected_d) {
      case '  Other  ':
        {
          superTypeSelectedVal_d = 0;
        }
        break;
      case '  City  ':
        {
          superTypeSelectedVal_d = 1;
        }
        break;
      case '  Rural  ':
        {
          superTypeSelectedVal_d = 2;
        }
        break;
      case '  Coastal  ':
        {
          superTypeSelectedVal_d = 3;
        }
        break;
      case '  Wilderness  ':
        {
          superTypeSelectedVal_d = 4;
        }
        break;
    }
  };

  printSuperSub_Active() {
    console.log('super type selected: ' + superTypeSelectedVal_d);
    console.log('Sub Pins: ');
    for (let i = 0; i < subPinArray_d.length; i++) {
      console.log(subPinArray_d[i].name + ': ' + subPinArray_d[i].active);
    }
  }

  renderButtons() {
    return subPinArray_d.map(item => {
      return (
        <SubPins
          _onPress={status => {
            item.active = status;
            for (let i = 0; i < subPinArray_d.length; i++) {
              if (subPinArray_d[i].active == true) subPinsActive_d[i] = 1;
              else if (subPinArray_d[i].active == false) subPinsActive_d[i] = 0;
            }
          }}
          text={item.name}
        />
      );
    });
  }

  render() {
    const { superType } = this.state;
    return (
      <View style={[{backgroundColor:'white'}, styles.container]}>
        <StatusBar barStyle="light-content" />

        <View style={styles.buffer} />
        <Text style={[{padding: 20},styles.labels]}>Location Types</Text>
        <View style={styles.buttonContainer}>
          <SuperPins
            items={[
              '  Other  ',
              '  City  ',
              '  Rural  ',
              '  Coastal  ',
              '  Wilderness  ',
            ]}
            value={superType}
            effect={'pulse'}
            onPressItem={this.handlePressItem}
          />
        </View>
        <View style={styles.buffer} />
        <Text style={[{padding: 20},styles.labels]}>Keywords</Text>
        <View style={styles.buttonContainer}>{this.renderButtons()}</View>
      </View>
    );
  }
}

export { subPinsActive_d };
export { superTypeSelected_d };
export { superTypeSelectedVal_d };
export { subPinArray_d };
export default AddTagsDrawer;
