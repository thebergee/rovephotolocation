//import React from 'react';
import React, { Component } from 'react';
import {
  Button,
  Image,
  Platform,
  View,
  Text,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  Alert,
  Header,
  ScrollView,
  StatusBar,
} from 'react-native';

import {
  get_SearchPinPrint,
  get_SearchSubPinPrint,
  get_SearchSuperPinPrint,
  get_PrintAllElementsInFirebase,
  set_LengthElementsInFirebase,
  get_LengthElementsInFirebase,
  set_ElementsInFirebase,
  get_ElementsInFirebase,
  get_PlaceInfo,
  set_NewPlace,
  get_SubPinType,
  set_SubPinType,
  get_SuperPinType,
  set_SuperPinType,
  get_ImgNum,
  set_ImgNum,
  get_userName,
  set_userName,
  set_firebaseImgTest,
  get_firebaseImgTest,
  t,
  uploadImageAsync,
  sm,
  f,
} from '../components/Functions';

import { createStackNavigator, createAppContainer } from 'react-navigation';
import MapView from 'react-native-maps';
import { Ionicons } from '@expo/vector-icons';

import SubPins from '../components/buttons/SubPins';
import SuperPins from '../components/buttons/SuperPins';

import { Divider } from 'react-native-paper';
import * as Font from 'expo-font';

import styles from '../components/Styles';
import { staticMethod, test } from '../components/Functions';

import { withNavigation } from 'react-navigation';
import { rList } from './LoadingScreenStart';

var superPinActive_S = [0, 0, 0, 0, 0];
var superPinArray_S = [
  {
    name: '  Other  ',
    active: false,
  },
  {
    //Landmark
    name: '  City  ',
    active: false,
  },
  {
    //Beach
    name: '  Rural  ',
    active: false,
  },
  {
    //Graffiti
    name: '  Coastal  ',
    active: false,
  },
  {
    //WildLife
    name: '  Wilderness  ',
    active: false,
  },
];

var subPinsActive_S = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var subPinArray_S = [
  {
    //Architecture
    name: '  Architecture  ',
    active: false,
  },
  {
    //Landmark
    name: '  Landmark  ',
    active: false,
  },
  {
    //Beach
    name: '  Beach  ',
    active: false,
  },
  {
    //Graffiti
    name: '  Graffiti  ',
    active: false,
  },
  {
    //WildLife
    name: '  WildLife  ',
    active: false,
  },
  {
    //Transport
    name: '  Transport  ',
    active: false,
  },
  {
    //Abandoned
    name: '  Abandoned  ',
    active: false,
  },
  {
    //Entertainment
    name: '  Entertainment  ',
    active: false,
  },
  {
    //Commercial
    name: '  Commercial  ',
    active: false,
  },
  {
    //Residential
    name: '  Residential  ',
    active: false,
  },
];

function setSuperAndSub_Active() {
  for (let i = 0; i < subPinArray_S.length; i++) {
    if (subPinArray_S[i].active == true) subPinsActive_S[i] = 1;
    else if (subPinArray_S[i].active == false) subPinsActive_S[i] = 0;
  }
  for (let i = 0; i < superPinArray_S.length; i++) {
    if (superPinArray_S[i].active == true) superPinActive_S[i] = 1;
    else if (superPinArray_S[i].active == false) superPinActive_S[i] = 0;
  }
}

function printSuperSub_Active() {
  for (let i = 0; i < superPinArray_S.length; i++) {
    console.log(superPinArray_S[i].name + ': ' + superPinActive_S[i]);
  }
  for (let i = 0; i < subPinArray_S.length; i++) {
    console.log(subPinArray_S[i].name + ': ' + subPinsActive_S[i]);
  }
}

class SearchScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'Search',
      headerRight: (
        <Button
          onPress={() => {
            setSuperAndSub_Active(),
              printSuperSub_Active(),
              navigation.navigate('Loading_Start');
          }}
          title="Done"
          color="#fff"
        />
      ),
    };
  };

  componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      {
        console.log('Heriiiiiiiiiiiiiiiiiiiiiie');
      }

      subPinsActive_S = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
      superPinActive_S = [0, 0, 0, 0, 0];

      console.log(subPinsActive_S);
      console.log(superPinActive_S);
      console.log(superPinArray_S);
    });
  }

  componentWillUnmount() {
    // Remove the event listener
    subPinArray_S = [
      {
        //Architecture
        name: '  Architecture  ',
        active: false,
      },
      {
        //Landmark
        name: '  Landmark  ',
        active: false,
      },
      {
        //Beach
        name: '  Beach  ',
        active: false,
      },
      {
        //Graffiti
        name: '  Graffiti  ',
        active: false,
      },
      {
        //WildLife
        name: '  WildLife  ',
        active: false,
      },
      {
        //Transport
        name: '  Transport  ',
        active: false,
      },
      {
        //Abandoned
        name: '  Abandoned  ',
        active: false,
      },
      {
        //Entertainment
        name: '  Entertainment  ',
        active: false,
      },
      {
        //Commercial
        name: '  Commercial  ',
        active: false,
      },
      {
        //Residential
        name: '  Residential  ',
        active: false,
      },
    ];

    superPinArray_S = [
      {
        name: '  Other  ',
        active: false,
      },
      {
        //Landmark
        name: '  City  ',
        active: false,
      },
      {
        //Beach
        name: '  Rural  ',
        active: false,
      },
      {
        //Graffiti
        name: '  Coastal  ',
        active: false,
      },
      {
        //WildLife
        name: '  Wilderness  ',
        active: false,
      },
    ];

    this.focusListener.remove();
  }

  renderButtonsSubPins() {
    return subPinArray_S.map(item => {
      return (
        <SubPins
          _onPress={status => {
            item.active = status;
          }}
          text={item.name}
        />
      );
    });
  }
  renderButtonsSuperPins() {
    return superPinArray_S.map(thing => {
      return (
        <SubPins
          _onPress={status => {
            thing.active = status;
          }}
          text={thing.name}
        />
      );
    });
  }
  render() {
    return (
      <View style={[{backgroundColor:'white'}, styles.container]}>
        <StatusBar barStyle="light-content" />
        <View style={styles.buffer} />
        <Text style={[{padding: 20},styles.labels]}>Location Types</Text>
        <View style={styles.buttonContainer}>
          {this.renderButtonsSuperPins()}
        </View>
        <View style={styles.buffer} />
        <Text style={[{padding: 20},styles.labels]}>Keywords</Text>
        <View style={styles.buttonContainer}>
          {this.renderButtonsSubPins()}
        </View>
      </View>
    );
  }
}
export { subPinsActive_S };
export { superPinActive_S };
export { superPinArray_S };
export { subPinArray_S };
export default withNavigation(SearchScreen);
