//import React from 'react';
import React, { Component } from 'react';
import {
  Button,
  Image,
  Platform,
  View,
  Text,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
  TouchableHighlight,
  Alert,
  Header,
  ScrollView,
  StatusBar,ActivityIndicator
} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import MapView from 'react-native-maps';
import { Ionicons } from '@expo/vector-icons';

import HomeScreen from './HomeScreen';
import SearchScreen from './SearchScreen';
import * as Font from 'expo-font';

const defaultPin = require('../assets/PinAssets/DefaultPin.png');
const cityPin = require('../assets/PinAssets/CityPin.png');
const coastalPin = require('../assets/PinAssets/CoastalPin.png');
const ruralPin = require('../assets/PinAssets/RuralPin.png');
const woodlandsPin = require('../assets/PinAssets/WoodlandsPin.png');

import { subPinsActive_S } from './SearchScreen';
import { superPinActive_S } from './SearchScreen';


import { withNavigation } from "react-navigation";

import {
  get_SearchPinPrint,
  get_SearchSubPinPrint,
  get_SearchSuperPinPrint,
  get_PrintAllElementsInFirebase,
  set_LengthElementsInFirebase,
  get_LengthElementsInFirebase,
  set_ElementsInFirebase,
  get_ElementsInFirebase,
  get_PlaceInfo,
  set_NewPlace,
  get_SubPinType,
  set_SubPinType,
  get_SuperPinType,
  set_SuperPinType,
  get_ImgNum,
  set_ImgNum,
  get_userName,
  set_userName,
  set_firebaseImgTest,
  get_firebaseImgTest,
  t,
  uploadImageAsync,
  sm,
  f,
} from '../components/Functions';

import {
  DISABLED_TOGGLE_BUTTON,
  ACTIVE_TOGGLE_BUTTON_C1,
  ACTIVE_TOGGLE_BUTTON_C2,
  FONT_SIZE,
} from '../components/Styles';
import styles from '../components/Styles';

import * as firebase from 'firebase';

var textT = ' ';

var rList = [];

var sN = 0;

var loadingNow = true;

class LoadingStartScreen extends React.Component {

  static navigationOptions = ({ navigation }) => {
     return {
          header: null 
    }
  };

   async componentDidMount() {
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      {console.log('Here')}

              {console.log(
          'Taken Input: ' + superPinActive_S[0],
          superPinActive_S[1],
          superPinActive_S[2],
          superPinActive_S[3],
          superPinActive_S[4],
          subPinsActive_S[0],
          subPinsActive_S[1],
          subPinsActive_S[2],
          subPinsActive_S[3],
          subPinsActive_S[4],
          subPinsActive_S[5],
          subPinsActive_S[6],
          subPinsActive_S[7],
          subPinsActive_S[8],
          subPinsActive_S[9]
        )}

{ this._getccSearchPinPrint(
              superPinActive_S[0],
              superPinActive_S[1],
              superPinActive_S[2],
              superPinActive_S[3],
              superPinActive_S[4],
              subPinsActive_S[0],
              subPinsActive_S[1],
              subPinsActive_S[2],
              subPinsActive_S[3],
              subPinsActive_S[4],
              subPinsActive_S[5],
              subPinsActive_S[6],
              subPinsActive_S[7],
              subPinsActive_S[8],
              subPinsActive_S[9]
            )}




    });
  }

    componentWillUnmount() {
    // Remove the event listener
    this.focusListener.remove();
  }


  state = {
    isLoading: false,
  };

  render() {
    return (
      <View style={[styles.container_loading]}>
      <StatusBar barStyle="light-content" style={styles.statusbarSty}/>
      <MapView
          style={{ flex: 1 }}
          region={{
            latitude: 42.666967,
            longitude: -71.123196,
            latitudeDelta: 0,
            longitudeDelta: 0,
          }}
          showsUserLocation={true}>
          </MapView>
          <View style={styles.curtain_loading}/>
      <Image
        source={require('../RoveLogos/Rove_logo_wht.png')}
        style={styles.roveLogo_Loading}
      />
      <ActivityIndicator style={styles.statusbarSty} size="large" color="white" />
      <View style={styles.loadingText}>
      <Text style={styles.baseText_loading}>
          Loading
      </Text>
      </View>
      </View>
    );
  }

  _getccSearchPinPrint = async (
    searchNum,
    searchNum1,
    searchNum2,
    searchNum3,
    searchNum4,
    sub0,
    sub1,
    sub2,
    sub3,
    sub4,
    sub5,
    sub6,
    sub7,
    sub8,
    sub9
  ) => {

      //console.log("IIIIINNNNNN")

      rList = [];


      textT = ' ';

      var pinImg;
      var corHolder;
      var el;
      var en;
      var check = false;

      for (var i = 0; i < (await get_LengthElementsInFirebase()); i++) {
        console.log('Line: 210');
        el = await get_ElementsInFirebase(i);
        console.log('Line: 212'); 
        en = await get_SuperPinType(el);
        console.log('Line: 214'); 

        check = false;

        if (
          searchNum == 0 &&
          searchNum1 == 0 &&
          searchNum2 == 0 &&
          searchNum3 == 0 &&
          searchNum4 == 0 &&
          sub0 == 0 &&
          sub1 == 0 &&
          sub2 == 0 &&
          sub3 == 0 &&
          sub4 == 0 &&
          sub5 == 0 &&
          sub6 == 0 &&
          sub7 == 0 &&
          sub8 == 0 &&
          sub9 == 0
        ) {
          console.log('blank all true')
          check = true;
        } else {
          //console.log('in search')
          if (
            (0 == en && searchNum == 1) ||
            (1 == en && searchNum1 == 1) ||
            (2 == en && searchNum2 == 1) ||
            (3 == en && searchNum3 == 1) ||
            (4 == en && searchNum4 == 1)
          ) {
            check = true;
          } else {
            if (sub0 == 1) {
              if (sub0 == (await get_SubPinType(el, 0))) {
                check = true;
              }
            } else if (sub1 == 1) {
              if (sub1 == (await get_SubPinType(el, 1))) {
                check = true;
              }
            } else if (sub2 == 1) {
              if (sub2 == (await get_SubPinType(el, 2))) {
                check = true;
              }
            } else if (sub3 == 1) {
              if (sub3 == (await get_SubPinType(el, 3))) {
                check = true;
              }
            } else if (sub4 == 1) {
              if (sub4 == (await get_SubPinType(el, 4))) {
                check = true;
              }
            } else if (sub5 == 1) {
              if (sub5 == (await get_SubPinType(el, 5))) {
                check = true;
              }
            } else if (sub6 == 1) {
              if (sub6 == (await get_SubPinType(el, 6))) {
                check = true;
              }
            } else if (sub7 == 1) {
              if (sub7 == (await get_SubPinType(el, 7))) {
                check = true;
              }
            } else if (sub8 == 1) {
              if (sub8 == (await get_SubPinType(el, 8))) {
                check = true;
              }
            } else if (sub9 == 1) {
              if (sub9 == (await get_SubPinType(el, 9))) {
                check = true;
              }
            }
          }
        }

        if (check) {
          corHolder = el.split(', ');

          switch (en) {
            case 0:
              await rList.push({
                title: 'hello',
                img: defaultPin,
                superPin: en,
                coordinates: {
                  latitude: await corHolder[0],
                  longitude: await corHolder[1],
                },
              });
              break;
            case 1:
              await rList.push({
                title: 'hello',
                img: cityPin,
                superPin: en,
                coordinates: {
                  latitude: await corHolder[0],
                  longitude: await corHolder[1],
                },
              });
              break;
            case 2:
              await rList.push({
                title: 'hello',
                img: ruralPin,
                superPin: en,
                coordinates: {
                  latitude: await corHolder[0],
                  longitude: await corHolder[1],
                },
              });
              break;
            case 3:
              await rList.push({
                title: 'hello',
                img: coastalPin,
                superPin: en,
                coordinates: {
                  latitude: await corHolder[0],
                  longitude: await corHolder[1],
                },
              });
              break;
            case 4:
              await rList.push({
                title: 'hello',
                img: woodlandsPin,
                superPin: en,
                coordinates: {
                  latitude: await corHolder[0],
                  longitude: await corHolder[1],
                },
              });
              break;
            default:
              await rList.push({
                title: 'hello',
                img: defaultPin,
                superPin: en,
                coordinates: {
                  latitude: await corHolder[0],
                  longitude: await corHolder[1],
                },
              });
              break;
          }

          console.log((await el) + ' Works in search ' + i);
        } else {
          console.log((await el) + ' Not in search' + i);
        }
      }

      console.log('List is :' + rList.length);
      console.log('Done');


        this.props.navigation.navigate('Home');
      

  };
}

export { rList };
export default withNavigation(LoadingStartScreen);
