//import React from 'react';
import React, { Component } from 'react';
import {
  Button,
  Image,
  Platform,
  View,
  Text,
  ImageBackground,
  Dimensions,
  Alert,
  Header,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TouchableWithFeedback,
} from 'react-native';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import MapView from 'react-native-maps';
import Marker from 'react-native-maps';
import { Ionicons } from '@expo/vector-icons';

import * as firebase from 'firebase';
import firebaseSvc from '../components/FirebaseSvc';

import * as Font from 'expo-font';
import uuid from 'uuid';
import styles from '../components/Styles';

import {
  get_SearchPinPrint,
  get_SearchSubPinPrint,
  get_SearchSuperPinPrint,
  get_PrintAllElementsInFirebase,
  set_ElementsInFirebase,
  get_ElementsInFirebase,
  get_PlaceInfo,
  set_NewPlace,
  get_SubPinType,
  set_SubPinType,
  get_SuperPinType,
  set_SuperPinType,
  get_ImgNum,
  set_ImgNum,
  get_userName,
  set_userName,
  set_firebaseImgTest,
  get_firebaseImgTest,
  t,
  uploadImageAsync,
  sm,
  f,
} from '../components/Functions';

import { subPinsActive_S } from './SearchScreen';
import { superPinActive_S } from './SearchScreen';
import { superPinArray_S } from './SearchScreen';
import { subPinArray_S } from './SearchScreen';

import defaultPin from '../assets/PinAssets/DefaultPin.png';

import * as Animatable from 'react-native-animatable';
import Icon from 'react-native-vector-icons/FontAwesome';

import { rList } from './LoadingScreenStart';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import NavigationBar from 'react-native-navbar';

var sendCorPin = 'helo';

const is_iOS = Platform.OS === 'ios';

class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: <LogoTitle />,
      headerRight: (
        <Button
          onPress={() => navigation.navigate('Search')}
          title="Search"
          justifyContent="center"
          color="#fff"
        />
      ),
      headerLeft: (
        <Button
          onPress={() => navigation.navigate('Loading_Start')}
          title=" Reload"
          justifyContent="center"
          color="#fff"
        />
      )
    };
  };

  state = {
    focusedLocation: {
      latitude: 42.666967,
      longitude: -71.123196,
      latitudeDelta: 0.0122,
      longitudeDelta:
        Dimensions.get("window").width /
        Dimensions.get("window").height *
        0.0122
    },
    locationChosen: false
  };
  _onPress() {
    //https://github.com/oblador/react-native-vector-icons#animation
    this.props.navigation.navigate('Add_pin', {});
  }

  pickLocationHandler = event => {
    const coords = event.nativeEvent.coordinate;
    this.map.animateToRegion({
      ...this.state.focusedLocation,
      latitude: coords.latitude,
      longitude: coords.longitude
    });
    this.setState(prevState => {
      return {
        focusedLocation: {
          ...prevState.focusedLocation,
          latitude: coords.latitude,
          longitude: coords.longitude
        },
        locationChosen: true
      };
    });
  };

  getLocationHandler = () => {
    navigator.geolocation.getCurrentPosition(pos => {
      const coordsEvent = {
        nativeEvent: {
          coordinate: {
            latitude: pos.coords.latitude,
            longitude: pos.coords.longitude
          }
        }
      };
      this.pickLocationHandler(coordsEvent);
    },
  err => {
    console.log(err);
    alert("Fetching the Position failed, please pick one manually!");
  })
  }
  
  render() {
    let marker = null;

    if (this.state.locationChosen) {
      marker = <MapView.Marker coordinate={this.state.focusedLocation} />;
    }
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />

        <MapView
          initialRegion={this.state.focusedLocation}
          style={styles.map}
          ref={ref => this.map = ref}
        showsUserLocation={true}>
          {rList.map(marker => (
            <MapView.Marker
              flat={true}
              image={marker.img}
              coordinate={marker.coordinates}
              onPress={() => {
                this.props.navigation.navigate('ViewMapPinScreen', {}),
                  console.log(marker.coordinates),
                  (sendCorPin =
                    marker.coordinates.latitude +
                    ', ' +
                    marker.coordinates.longitude),
                  console.log('sendIs' + sendCorPin);
              }}
            />
          ))}
        </MapView>
        <View
          style={[
            {
              position: 'absolute',
              top: Dimensions.get('screen').height - 212,
              left: 26,
              zIndex: 100,
            },
          ]}>
          <TouchableOpacity onPress={() => this._onPress()}>
            <Icon
              name={'plus-circle'}
              color={'#1d1d27'}
              backgroundColor="transparent"
              size={70}
            />
          </TouchableOpacity>
        </View>
        <View
          style={[
            {
              position: 'absolute',
              top: Dimensions.get('screen').height - 212,
              left: 26,
              zIndex: 1,
            },
          ]}>
          <Icon
            name={'circle'}
            color={'white'}
            style={'solid'}
            backgroundColor={'pink'}
            size={70}
          />
        </View>
      </View>
    );
  }
}


class LogoTitle extends React.Component {
  render() {
    return (
      <Image
        source={require('../RoveLogos/Rove_logo_wht.png')}
        style={styles.rove_headersty}
      />
    );
  }
}
export { sendCorPin };
export default HomeScreen;
