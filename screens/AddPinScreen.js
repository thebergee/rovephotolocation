//import React from 'react';
import React from 'react';
import {
  View,
  Dimensions,
  StatusBar,
} from 'react-native';
import MapView, {
  Marker,
  ProviderPropType,
} from 'react-native-maps';

import styles from '../components/Styles';

import defaultPin from '../assets/PinAssets/DefaultPin.png';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE = 42.666967;
const LONGITUDE = -71.123196;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

var latitude_m = 100;
var longitude_m = -100;

class AddPinScreen extends React.Component {
  static navigationOptions = () => {
    return {
      headerTitle: 'Add Pin',
      fontFamily: 'Montserrat-Regular'
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      marker: {
        coordinate: undefined,
      },
    };
    this.onMapPress = this.onMapPress.bind(this);
  }

  onMapPress(e) {
    latitude_m= e.nativeEvent.coordinate.latitude
   longitude_m= e.nativeEvent.coordinate.longitude
    this.setState({
      marker: {
        coordinate: e.nativeEvent.coordinate,
      },
    });
    
  }

  render() {
    return (
      <View style={[{backgroundColor:'white'}, styles.container]}>
        <StatusBar barStyle="light-content" />
        <MapView
          provider={this.props.provider}
          style={styles.map}
          onLongPress={this.onMapPress}>
          <Marker
            image={defaultPin}
            coordinate={this.state.marker.coordinate}
            onPress={() => { 
            this.props.navigation.navigate('Add_tags_drawer', {})}}
          />
        </MapView>
      </View>
    );
  }
}
//onPress={() => console.log(this.state.marker)}
AddPinScreen.propTypes = {
  provider: ProviderPropType,
};

export { latitude_m }
export { longitude_m }
export default AddPinScreen;