import React, {Component} from 'react';
import { View, TouchableOpacity, StatusBar, ScrollView } from 'react-native';


import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker'


import Icon from 'react-native-vector-icons/FontAwesome';

import AsyncImage from '../components/AsyncImage';

import styles from '../components/Styles';
import Svg, {Rect} from 'react-native-svg';

import { latitude_m } from './AddPinScreen';
import { longitude_m } from './AddPinScreen';

//importing functions
import {
  get_ImgNum,
  set_ImgNum,
  set_firebaseImgTest,
  get_firebaseImgTest,
  uploadImageAsync,
} from '../components/Functions';

//var currentPinViewPin = '-1_7963001297487344, -47_09714115020705';

import { sendCorPin } from './HomeScreen';
import { withNavigation } from 'react-navigation';

var i;
var currentPinViewPin = sendCorPin;

class ViewMapPinScreen extends Component {
  state = {
    bodyText: 'T',
    bodyTextTwo: ' ',
    image: null,
    yimUpload: 'https://www.gstatic.com/webp/gallery/2.jpg',
    imageUri: 'T',
    imageUri1: 'T',
    imageUri2: 'T',
    imageUri3: 'T',
    imageUri4: 'T',
    imageUri5: 'T',
    imgNum: 1,
    imageUrilist: [],
  };

  async componentDidMount() {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);

    console.log('F---------------:' + sendCorPin);

    currentPinViewPin = sendCorPin;

    await get_ImgNum(currentPinViewPin)
      .then(imgNum => this.setState({ imgNum }))
      .catch(imgNum => this.setState({ imgNum }));

    this.state.imageUrilist = [];

    await get_firebaseImgTest(currentPinViewPin, 1, this.state.imgNum)
      .then(imageUri => this.setState({ imageUri }))
      .catch(imageUri => this.setState({ imageUri }));
    this.state.imageUrilist[this.state.imgNum] = this.state.imageUri;

    for (i = 0; i < this.state.imgNum; i++) {
      await get_firebaseImgTest(
        currentPinViewPin,
        this.state.imgNum - i,
        this.state.imgNum
      )
        .then(imageUri => this.setState({ imageUri }))
        .catch(imageUri => this.setState({ imageUri }));

      this.state.imageUrilist[i] = this.state.imageUri;
      console.log(this.state.imgNum + 'imj ' + i);
    }
  }


    componentWillUnmount() {
    i = this.state.imgNum;
  }
  
  static navigationOptions = () => {
    return {
      headerTitle: 'Gallery',
    };
  };

  render() {
    return (     

      <ScrollView style={styles.viewImageContainer}>
        <StatusBar barStyle="light-content" />
        <View style={{ flex: 1, flexDirection: 'row', flexWrap: 'wrap' }}>
          <View style={styles.addContainer}>
            <Svg width="200" height="200">
              <Rect
                x="5%"
                y="5%"
                width="90%"
                height="90%"
                fill="black"
                strokeWidth="4"
                stroke="white"
                strokeDasharray="5,5"
              />
            </Svg>
            <View
              style={[
                {
                  position: 'absolute',
                  zIndex: 100,
                },
              ]}>
              <TouchableOpacity onPress={this._BBpickImage}>
                <Icon
                  name={'plus-circle'}
                  color={'#0A89C2'}
                  backgroundColor={'transparent'}
                  size={70}
                />
              </TouchableOpacity>
            </View>
            <View
              style={[
                {
                  position: 'absolute',
                  zIndex: 1,
                },
              ]}>
              <Icon
                name={'circle'}
                color={'white'}
                style={'solid'}
                backgroundColor={'pink'}
                size={70}
              />
            </View>
          </View>
          {this.state.imageUrilist.map(marker => (
            <AsyncImage
              style={styles.addContainer}
              source={{
                uri: marker,
              }}
              placeholderColor="#000000"
            />
          ))}
        </View>
      </ScrollView>
    );
  }

  _firstimage = () => {
    let { image } = this.state;
    if (!image) {
      console.log('https://www.gstatic.com/webp/gallery/2.jpg');
      return;
    } else {
      console.log("HELLLOOOOOOOO")
      console.log(image)
      return image;
    }
  };z

  _maybeRenderImage = targetpin => {
    let { image } = this.state;
    if (!image) {
      console.log('Image Fail Rend');
      return;
    } else {
      if (image != this.state.imageUri) {
        set_ImgNum(targetpin, this.state.imgNum + 1);
        set_firebaseImgTest(targetpin, image, this.state.imgNum + 1);
        {
          this.componentDidMount();
        }
      }
    }
  };

  _pickImage = async () => {
    console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    this._handleImagePicked(pickerResult);
  };

  _handleImagePicked = async pickerResult => {
    console.log('wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww');
    try {
      this.setState({ uploading: true });

      if (!pickerResult.cancelled) {
        //  console.log("in thing");
        var uploadUrl = await uploadImageAsync(pickerResult.uri);
        this.setState({ image: uploadUrl });
      }
    } catch (e) {
      console.log(e);
      alert('Something went wrong in upload');
    } finally {
      this.setState({ uploading: false });
    }
  };

  _BBpickImage = async () => {
    console.log('xxxxxxxxxxxxxxxxxxxxxxxxxxxx');
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });
    this._BBhandleImagePicked(pickerResult);
  };

  _BBhandleImagePicked = async pickerResult => {
    console.log('nnnnnnnnnnnnnnnnnnnnnnnnnnn');
    try {
      this.setState({ uploading: true });

      if (!pickerResult.cancelled) {
        //  console.log("in thing");
        var uploadUrl = await uploadImageAsync(pickerResult.uri);
        this.setState({ image: uploadUrl });
        this.setState({ yimUpload: uploadUrl });
        this._maybeRenderImage(currentPinViewPin);
      }
    } catch (e) {
      console.log(e);
      alert('Something went wrong in upload');
    } finally {
      this.setState({ uploading: false });
    }
  };
}

export { currentPinViewPin };
export default withNavigation(ViewMapPinScreen);

