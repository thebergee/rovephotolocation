import React from 'react';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import * as Font from 'expo-font';

import LoadingStartScreen from './screens/LoadingScreenStart'
import HomeScreen from './screens/HomeScreen'
//import HelpScreen from './screens/HelpScreen'
import SearchScreen from './screens/SearchScreen'
import AddPinScreen from './screens/AddPinScreen'
import AddTagsDrawer from './screens/AddTagsDrawer'
import ViewMapPinScreen from './screens/ViewMapPinScreen'
import ViewPinImagesScreen from './screens/ViewPinImagesScreen'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons'

library.add(faStroopwafel);

const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
      },
      //Help: {
        //screen: HelpScreen,
        //},
    Search: {
      screen: SearchScreen,
      },
    Add_pin: {
    screen: AddPinScreen,
    },
    View_pin_images: {
      screen: ViewPinImagesScreen,
      },
      Add_tags_drawer: {
        screen: AddTagsDrawer,
        },
    Loading_Start: {
      screen: LoadingStartScreen,
    },
    ViewMapPinScreen: {
      screen: ViewMapPinScreen,
      },
  },
  {
    initialRouteName: 'Loading_Start',
    defaultNavigationOptions: {
      gesturesEnabled: false,
      headerStyle: {
        backgroundColor: '#1d1d27',
      },
      headerTintColor: 'white',
      headerTitleStyle: {
        fontFamily: 'Montserrat-Regular',
        fontSize: 28,
        fontWeight: 'regular',
      },
      headerBackTitleStyle: {
        fontFamily: 'Montserrat-Regular',
      },
    },
  }
);
//Zapfino
//Montserrat-Regular
//Bodoni Ornaments
//PartyLetPlain
//'Party LET'
const AppContainer = createAppContainer(RootStack);

export default class App extends React.Component {
  async componentDidMount() {
    await Font.loadAsync({
      'Montserrat-Regular': require('./assets/fonts/Montserrat/Montserrat-Regular.ttf'),
    });
    this.UNSAFE_componentWillMountsetState({ fontLoaded: true });
  }
  render() {
    return <AppContainer />;
  }
}